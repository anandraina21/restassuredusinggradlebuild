import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class BeersApiTest extends Setup {

	/**
	 * Test if the response is having id, name, description, abv.
	 */
	void checkResponseFields(Response res) {

		List<Object> list;
		list = res.jsonPath().getList("$");
		int responseObjectsSize = list.size();
		System.out.println("JsonResponse Size: " + responseObjectsSize + "\n");

		String[] params = { "id", "name", "description", "abv" };
		Object paramValue;
		for (String param : params) {
			list = res.jsonPath().getList(param);
			int checkParamInListSize = list.size();
			Assert.assertEquals(checkParamInListSize, responseObjectsSize, "Mismatch in size found in JSON for " + param + "!");
			for(int i = 0; i < checkParamInListSize; i++) {
				paramValue = list.get(i);
				Assert.assertNotNull(paramValue, "Object " + param + "[" + i + "] is null!");
			}
			System.out.println("JSON object validation successful for " + param + "!");
		}
	}


	/**
	 * Test if the end point "https://api.punkapi.com/v2/beers" is working.
	 * Test if the response is having id, name, description, abv.
	 *
	 */
	@Test(priority = 1, groups = {"smoke", "regression"})
	public void getAllBeers() {
		System.out.println("===============getAllBeers test started===============\n");
		Response res = req.baseUri(Constants.BEERS_BASE_URL).contentType("application/json").get();
		res.then().log().all();
		System.out.println();

		// checking the end point is working or not
		int statusCode = res.getStatusCode();
		System.out.println("Endpoint returned with code : " + statusCode + "\n");
		Assert.assertEquals(statusCode, 200, "Unsuccessful request!");

		// test if response have id, name, description, abv
		checkResponseFields(res);

		System.out.println("===============getAllBeers test completed===============\n");
	}
	
	
	
	/**
	 * Test if the end point "https://api.punkapi.com/v2/beers?brewed_before=<given date>" is returning only beers which are brewed before the given date.
	 * Test if the response is having id, name, description, abv.
	 * @throws CustomException 
	 */
	@Test(priority = 2, groups = {"regression"})
	public void getAllBeersBrewedBeforeDate() throws CustomException {
		System.out.println("===============getAllBeersBrewedBeforeDate test started===============\n");
		String givenBrewedBeforeDate = "02-2011";
		Response res = req.baseUri(Constants.BEERS_BASE_URL).contentType("application/json").param("brewed_before", givenBrewedBeforeDate).get();
		res.then().log().all();
		System.out.println();
		int statusCode = res.getStatusCode();
		System.out.println("Endpoint returned with code : " + statusCode + "\n");
		Assert.assertEquals(statusCode, 200, "Unsuccessful request!");


		// test if endpoint is returning only beers which are brewed before the given date

		System.out.println("Endpoint date: " + givenBrewedBeforeDate);
		int givenBrewedBeforeMonth = Integer.parseInt(givenBrewedBeforeDate.split("-")[0]);
		int givenBrewedBeforeYear = Integer.parseInt(givenBrewedBeforeDate.split("-")[1]);
		List<Object> list;
		list = res.jsonPath().getList("first_brewed");
		int listSize = list.size();
		for(int i = 0; i < listSize; i++) {
			String actualBrewedBeforeDate = list.get(i).toString();
			System.out.println("Output Date in JSON at object " + (i+1) + ": " + actualBrewedBeforeDate);
			int actualBrewedBeforeMonth = Integer.parseInt(actualBrewedBeforeDate.split("/")[0]);
			int actualBrewedBeforeYear = Integer.parseInt(actualBrewedBeforeDate.split("/")[1]);
			if(!(actualBrewedBeforeYear < givenBrewedBeforeYear)) {
				if(actualBrewedBeforeYear == givenBrewedBeforeYear) {
					if(!(actualBrewedBeforeMonth < givenBrewedBeforeMonth)) {
						throw new CustomException("Month validation failed!");
					}
				} else if(actualBrewedBeforeYear > givenBrewedBeforeYear) {
					throw new CustomException("Year validation failed!");
				}
			}
			
		}
		
		System.out.println("Validation successful for checking if endpoint is returning only beers which are brewed before the given date!\n");
		
		
		// test if response have id, name, description, abv
		checkResponseFields(res);

		System.out.println("===============getAllBeersBrewedBeforeDate test completed===============\n");
	}
	
	
	
	/**
	 * Test if the end point "https://api.punkapi.com/v2/beers?abv_gt=6" is returning only beers which have abv > 6
	 * Test if the response is having id, name, description, abv
	 * @throws CustomException 
	 */
	@Test(priority = 3, groups = {"regression"})
	public void getAllBeersAbv() throws CustomException {
		System.out.println("===============getAllBeersAbv test started===============\n");
		String sGivenAbv = "6";
		Response res = req.baseUri(Constants.BEERS_BASE_URL).contentType("application/json").param("abv_gt", sGivenAbv).get();
		res.then().log().all();
		System.out.println();
		int statusCode = res.getStatusCode();
		System.out.println("Endpoint returned with code : " + statusCode + "\n");
		Assert.assertEquals(statusCode, 200, "Unsuccessful request!");


//		Test if the end point "https://api.punkapi.com/v2/beers?abv_gt=6" is returning only beers which have abv > 6
		System.out.println("Endpoint abv: " + sGivenAbv);
		float iGivenAbv = Float.parseFloat(sGivenAbv);
		List<Object> list;
		list = res.jsonPath().getList("abv");
		int listSize = list.size();
		for(int i = 0; i < listSize; i++) {
			float actualAbv = Float.parseFloat(list.get(i).toString());
			System.out.println("Abv for object " + (i+1) + ": " + actualAbv);
			if(!(actualAbv > iGivenAbv)) {
				throw new CustomException("Mismatch found!");
			}
		}
		System.out.println("Validation successful for abv > 6!\n");
		
		// test if response have id, name, description, abv
		checkResponseFields(res);

		
		System.out.println("===============getAllBeersAbv test completed===============\n");
	}
	
	
	
	/**
	 * Test if the end point "https://api.punkapi.com/v2/beers?page=2&per_page=5" is returning only 5 beers in the second page
	 * Test if the response is having id, name, description, abv
	 */
	@Test(priority = 4, groups = "regression")
	public void verifyPagination() {
		System.out.println("===============verifyPagination test started===============\n");
		String sGivenPage = "2";
		String sGivenPerPage = "5";
		Response res = req.baseUri(Constants.BEERS_BASE_URL).contentType("application/json").param("page", sGivenPage).param("per_page", sGivenPerPage).get();
		res.then().log().all();
		System.out.println();
		int statusCode = res.getStatusCode();
		System.out.println("Endpoint returned with code : " + statusCode + "\n");
		Assert.assertEquals(statusCode, 200, "Unsuccessful request!");


		// test endpoint returning 5 beers & if response have id, name, description, abv
		checkResponseFields(res);

		
		System.out.println("===============verifyPagination test completed===============");
	}
}
